@ECHO OFF

SET SrcDir=src
SET OutDir=bin
SET ClassPath=ie.ul.estore
SET MainClass=Application
SET MainClassDir=ie\ul\estore
SET JarFile=estore.jar

REM Remove the build dir if it already exists
DEL "%JarFile%"
RD /S /Q "%OutDir%"
MD "%OutDir%"

REM Compile all the java files and output classes into ./bin
javac -d %OutDir% -sourcepath %SrcDir% ^ "%SrcDir%\%MainClassDir%\%MainClass%.java" 

REM Make a jar file out of the classes
jar cvfe %JarFile% %ClassPath%.%MainClass% -C %OutDir% .
