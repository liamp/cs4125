package ie.ul.estore;

import ie.ul.estore.swingui.SwingView;


public final class Redirect {
    
    public static final class Back { };
    
    // TODO Change me to use the abstract {@link View} class!
    public final SwingView view;
    
    public Redirect(final SwingView newView) {
        this.view = newView;
    }
}
