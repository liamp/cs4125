package ie.ul.estore.auth;

import ie.ul.estore.product.CreateProductView;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.event.Dispatcher;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginView extends SwingView {

    private final JLabel usernameLabel;
    private final JLabel passwordLabel;
    private final JTextField usernameInput;
    private final JPasswordField passwordInput;
    private final JButton loginButton;
    private final JButton registerButton;

    public LoginView(Dispatcher d) {
        super(d, "Login");

        usernameLabel = new JLabel("Username");
        passwordLabel = new JLabel("Password");
        usernameInput = new JTextField();
        passwordInput = new JPasswordField();
        loginButton = new JButton("Login");
        registerButton = new JButton("Register");

        usernameLabel.setBounds(80, 70, 200, 30);
        passwordLabel.setBounds(80, 110, 200, 30);
        usernameInput.setBounds(300, 70, 200, 30);
        passwordInput.setBounds(300, 110, 200, 30);
        loginButton.setBounds(150, 160, 100, 30);
        registerButton.setBounds(250,160,100,30);


        this.addComponent(usernameLabel);
        this.addComponent(passwordLabel);
        this.addComponent(usernameInput);
        this.addComponent(passwordInput);
        this.addComponent(loginButton);
        this.addComponent(registerButton);
        
        this.loginButton.addActionListener(e -> getDispatcher().dispatch(new LoginAttemptEvent(getUsernameInput(), getPasswordInput())));
        this.registerButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new RegisterView(getDispatcher()))));
    }
    
    public String getUsernameInput() {
        return this.usernameInput.getText();
    }

    public String getPasswordInput() {
        return new String(this.passwordInput.getPassword());
    }
}
