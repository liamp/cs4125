package ie.ul.estore.auth;

import ie.ul.estore.Application;
import ie.ul.estore.Controller;
import ie.ul.estore.MessageEvent;
import ie.ul.estore.data.Repository;
import ie.ul.estore.event.Responds;
import ie.ul.estore.store.Store;
import ie.ul.estore.user.DashboardView;
import ie.ul.estore.user.UserAccount;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AuthController extends Controller {

    private final Repository<UserAccount> users;

    public AuthController(Application app, Repository<UserAccount> users) {
        super(app);
        this.users = users;
    }

    @Responds(to = LoginAttemptEvent.class)
    public void attemptLogin(LoginAttemptEvent login) {
        Optional<UserAccount> maybeUser = this.users.findFirst(u -> u.validateCredentials(login.username, login.password));
        if (maybeUser.isPresent()) {
            final UserAccount user = maybeUser.get();
            this.app.setLoggedInUser(user);
            final List<Store> storesList = this.app.getStores().all();
            final Store stores[] = storesList.toArray(new Store[storesList.size()]);
            this.dispatchRedirect(new DashboardView(app.getDispatcher(), user, stores));
        } else {
            this.dispatchErrorMessage("Invalid Credentials");
        }
    }

    @Responds(to = RegisterEvent.class)
    public void attemptRegister(RegisterEvent register) {
        String pattern = ".*[a-zA-z]+[0-9]+.*$";
        Optional<UserAccount> maybeUser = this.users.findFirst(u -> u.getUsername().equals(register.username));
        if (maybeUser.isPresent()) {
            this.app.getDispatcher().dispatch(new MessageEvent(MessageEvent.ERROR, "User Already Exists"));
        } else {
            if (register.password.equals(register.passwordMatch) && (register.password.matches(pattern))) {
                final UserAccount user = new UserAccount(register.username, register.password, 0.0, "Default");
                try {
                    this.users.insert(user);
                    this.dispatchRedirect(new LoginView(this.app.getDispatcher()));

                } catch (IOException ex) {
                    this.dispatchErrorMessage("Application error occurred while processing your registration. Please try again later.");
                }

            } else {
                this.dispatchErrorMessage("Password must match");
            }
        }
    }

}
