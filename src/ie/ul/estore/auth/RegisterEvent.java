package ie.ul.estore.auth;

public final class RegisterEvent {
    
    public final String username;
    public final String password;
    public final String passwordMatch;
    
    public RegisterEvent(final String username, final String pass, final String passMatch) {
        this.username = username;
        this.password = pass;
        this.passwordMatch = passMatch;
    }
    
}
