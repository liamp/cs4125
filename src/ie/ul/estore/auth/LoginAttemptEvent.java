package ie.ul.estore.auth;

public final class LoginAttemptEvent {
    
    public final String username;
    public final String password;
    
    public LoginAttemptEvent(final String username, final String password) {
        this.username = username;
        this.password = password;
    }
}
