package ie.ul.estore.store;

import ie.ul.estore.product.Review;
import ie.ul.estore.product.Product;
import ie.ul.estore.product.CreateProductEvent;
import ie.ul.estore.product.ProductRatingEvent;
import ie.ul.estore.product.RemoveProductEvent;
import ie.ul.estore.Application;
import ie.ul.estore.Controller;
import ie.ul.estore.data.Repository;
import ie.ul.estore.event.Responds;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StoreController extends Controller {

    private final Repository<Store> stores;

    public StoreController(Application app) {
        super(app);
        this.stores = app.getStores();
    }

    @Responds(to = CreateProductEvent.class)
    public void createProduct(CreateProductEvent event) {

        Optional<Store> maybeStore = this.stores.findFirst(s -> s.getId() == event.addingUser.getId());

        if (maybeStore.isPresent()) {
            Store store = maybeStore.get();
            store.addProduct(new Product(event.name, event.price, event.description, event.categoryNames));
            try {
                this.stores.save();
            } catch (IOException ex) {
                this.dispatchErrorMessage("Error occurred while creating product");
            }
        } else {
            this.dispatchErrorMessage("Failed to create product");
        }
    }
    @Responds(to = RemoveProductEvent.class)
    public void removeProductFromStore(RemoveProductEvent event) 
    {
        Optional<Store> maybeStore = this.stores.findFirst(s -> s.getId() == event.removingUser.getId());
        if(maybeStore.isPresent()){
            Store store = maybeStore.get();
            store.removeProduct(event.product);
            try {
                this.stores.save();
                this.dispatchInfoMessage("Product " + event.product.getName() + " removed");
            } catch(IOException ex) {
                Logger.getLogger(StoreController.class.getName()).log(Level.SEVERE,null,ex);
            }
        }
        else {
            this.dispatchErrorMessage("Failed to remove product");
        }
    }
    @Responds(to = ProductRatingEvent.class)
    public void rateProduct(ProductRatingEvent event) {
        final Review review = new Review(event.reviewText, event.author, event.rating);
        event.product.addReview(review);
        try {
            this.stores.save();
        } catch (IOException ex) {
            this.dispatchErrorMessage("Failed to add your review");
        }

    }
}
