package ie.ul.estore.store;

import ie.ul.estore.product.Product;
import java.util.function.Predicate;

public interface Filter extends Predicate<Product> {
    default Filter or(Filter other) {
        return (t) -> test(t) || other.test(t); 
    }
}
