package ie.ul.estore.store;

import ie.ul.estore.product.Category;
import ie.ul.estore.product.Product;
import java.util.Collections;
import java.util.List;

public class CategoriesFilter implements Filter {
    private final List<Category> categories;

    public CategoriesFilter(List<Category> categories) {
        this.categories = categories;
    }
    
    @Override
    public boolean test(Product input) {
        return !Collections.disjoint(categories, input.getCategories());
    }
    
}
