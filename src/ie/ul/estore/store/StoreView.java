package ie.ul.estore.store;

import ie.ul.estore.product.Product;
import ie.ul.estore.product.ProductDetailsView;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class StoreView extends SwingView {

    private final UserAccount user;
    private final Store store;

    private final JLabel titleBar;
    private final JList<Product> products;

    public StoreView(Dispatcher d, UserAccount loggedInUser, Store store) {
        super(d, store.getTitle());

        this.user = loggedInUser;
        this.store = store;

        super.getContent().setLayout(new BorderLayout());
        this.titleBar = new JLabel(store.getTitle());
        this.addComponent(this.titleBar, BorderLayout.PAGE_START);

        this.products = new JList<>(store.getProducts().toArray(new Product[0]));
        this.addComponent(products, BorderLayout.CENTER);

        this.products.addListSelectionListener(new StoreView.ListSelectionHandler());
    }

    private final class ListSelectionHandler implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) { // When a store is selected, go to that store page.
            if (!e.getValueIsAdjusting()) {
                final Product selectedProduct = products.getSelectedValue();
                getDispatcher().dispatch(new Redirect(new ProductDetailsView(getDispatcher(), user, selectedProduct)));
            }
        }
    }

}
