package ie.ul.estore.store;

import ie.ul.estore.product.Category;
import ie.ul.estore.product.Product;
import ie.ul.estore.data.Model;
import ie.ul.estore.data.Relation;
import ie.ul.estore.user.UserAccount;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class Store extends Model {

    private final String storeName;
    private final ArrayList<Category> categories;
    private final Relation<UserAccount> user;
    private final List<Product> products;

    public Store(String storeName, List<Product> products, final UserAccount user) {
        this.categories = new ArrayList<>();
        this.storeName = storeName;
        this.products = products;
        this.user = Relation.hasOne(user);
    }

    public void newCategory(String name) {
        Category temp = new Category(name);
        categories.add(temp);
    }

    public String getTitle() {
        return this.storeName;
    }

    @Override
    public String toString() {
        return this.storeName;
    }

    public List<Product> getProducts() {
        return Collections.unmodifiableList(this.products);
    }
    
    public List<Product> getProducts(List<Filter> filters) {
        final Filter combinedFilters = filters.stream().reduce(t -> true, Filter::or);
        final List<Product> filteredList = this.products.stream().filter(combinedFilters).collect(Collectors.toList());
        
        return Collections.unmodifiableList(filteredList);
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }
    
    public void removeProduct(Product product){
        this.products.remove(product);
    }
}
