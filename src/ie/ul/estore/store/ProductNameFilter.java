package ie.ul.estore.store;

import ie.ul.estore.product.Product;

public class ProductNameFilter implements Filter {

    private final String searchText;

    public ProductNameFilter(String searchText) {
        this.searchText = searchText;
    }
    
    @Override
    public boolean test(final Product input) {
        return input.getName().toLowerCase().contains(searchText.toLowerCase());
    }
    
}
