package ie.ul.estore.user;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.user.Transaction;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JList;

public final class TransactionView extends SwingView {
    
    private final JLabel title;
    private final JList<Transaction> transactions;
    private final UserAccount user;
    
    public TransactionView(Dispatcher d, final UserAccount user) {
        super(d, "My Transactions");
        
        this.user = user;
        this.getContent().setLayout(new BorderLayout());
        this.title = new JLabel("Your transactions");
        this.transactions = new JList<>(user.getTransactions());
        
        this.getContent().add(this.title, BorderLayout.BEFORE_FIRST_LINE);
        this.getContent().add(this.transactions, BorderLayout.CENTER);
    }
}
