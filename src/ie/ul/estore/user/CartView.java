package ie.ul.estore.user;

import ie.ul.estore.Redirect;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.product.RemoveProductView;
import ie.ul.estore.product.Product;
import ie.ul.estore.checkout.CheckoutView;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.Cart;
import ie.ul.estore.user.UserAccount;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public final class CartView extends SwingView {
    
    private final JLabel title;
    private final JList<Product> productsInCart;
    private final JButton checkoutButton;
    private final UserAccount user;
    private final Cart cart;
    private final JButton transactionButton;
    
    public CartView(Dispatcher d, final UserAccount user) {
        super(d, "My Transactions");
        
        this.user = user;
        this.cart = user.getCart();
        this.getContent().setLayout(new BorderLayout());
        this.title = new JLabel("Your cart -  " + cart.getItemCount() + " items (€ " + cart.getTotal() + ")");
        this.productsInCart = new JList<>(cart.getItemsInCart());
        
        this.getContent().add(this.title, BorderLayout.BEFORE_FIRST_LINE);
        this.getContent().add(this.productsInCart, BorderLayout.CENTER);
        
        this.checkoutButton = new JButton("Checkout");
        this.getContent().add(this.checkoutButton, BorderLayout.AFTER_LINE_ENDS);
        this.checkoutButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new CheckoutView(getDispatcher(), this.user))));
        
        this.transactionButton = new JButton("Transactions");
        this.getContent().add(this.transactionButton, BorderLayout.PAGE_END);
        this.transactionButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new TransactionView(getDispatcher(), this.user))));

         this.productsInCart.addListSelectionListener(new CartView.ListSelectionHandler());
    }
    
        private final class ListSelectionHandler implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) { // When a store is selected, go to that store page.
            if (!e.getValueIsAdjusting()) {
                final Product selectedProduct = productsInCart.getSelectedValue();
                getDispatcher().dispatch(new Redirect(new RemoveProductView(getDispatcher(), user, selectedProduct)));
            }
        }
    }
}
