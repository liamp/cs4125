package ie.ul.estore.user;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.store.Store;
import ie.ul.estore.store.StoreView;
import ie.ul.estore.event.Dispatcher;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DashboardView extends SwingView {

    private final JLabel helloLabel;
    private final JLabel storesListTitle;
    private final JList<Store> storesList;
    private final JButton myAccountButton;
    private final UserAccount user;

    public DashboardView(Dispatcher d, UserAccount user, Store stores[]) {
        super(d, "Dashboard");

        super.getContent().setLayout(new BorderLayout());
        this.user = user;

        final JPanel headerContainer = new JPanel();
        headerContainer.setLayout(new BorderLayout());
        
        this.helloLabel = new JLabel("Hello " + user.getUsername() + " your current plan is " + user.getMembershipPlan().getName());
        this.helloLabel.setHorizontalAlignment(JLabel.CENTER);
        this.helloLabel.setBorder(new EmptyBorder(10, 0, 10, 0));
        headerContainer.add(helloLabel, BorderLayout.CENTER);
        
        this.myAccountButton = new JButton("My Account");
        myAccountButton.addActionListener(e -> d.dispatch(new Redirect(new AccountView(d, user))));
        headerContainer.add(myAccountButton, BorderLayout.EAST);
        this.addComponent(headerContainer, BorderLayout.PAGE_START);
        
        
        this.storesListTitle = new JLabel("Stores");
        this.addComponent(storesListTitle, BorderLayout.CENTER);
        
        this.storesList = new JList<>(stores);
        this.storesList.addListSelectionListener(new DashboardView.ListSelectionHandler());
        this.addComponent(storesList, BorderLayout.CENTER);

    }

    // Handles click events for items in the list
    private final class ListSelectionHandler implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) { // When a store is selected, go to that store page.
            if (!e.getValueIsAdjusting()) {
                final Store selection = storesList.getSelectedValue();
                getDispatcher().dispatch(new Redirect(new StoreView(getDispatcher(), user, selection)));
            }
        }
    }
}
