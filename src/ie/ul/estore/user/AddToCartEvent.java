package ie.ul.estore.user;

import ie.ul.estore.product.Product;
import ie.ul.estore.user.UserAccount;


public final class AddToCartEvent {
    public final UserAccount user;
    public final Product selectedProduct;
    
    public AddToCartEvent(final UserAccount user, final Product selectedProduct) {
        this.user = user;
        this.selectedProduct = selectedProduct;
    }
    
}
