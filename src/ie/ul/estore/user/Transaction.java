package ie.ul.estore.user;

import ie.ul.estore.data.Model;
import ie.ul.estore.user.UserAccount;
import java.util.Date;

public class Transaction extends Model{
    private final double amount;
    private final Date transactionDate;

    public Transaction(final Date transactionDate, double amount) {
        this.amount = amount;
        this.transactionDate = transactionDate;
    }

    public void commit() {
        // TODO Persist transaction here.
    }

    public String getDetails() {
        // TODO Return string representation of transaction
        return null;
    }
    
    @Override
    public String toString() {
        return "Purchase: " + this.transactionDate + " " + this.amount;
    }
  
}
