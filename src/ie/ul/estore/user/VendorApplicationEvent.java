package ie.ul.estore.user;

public class VendorApplicationEvent {

    public final String billingAddress;
    public final String billingCard;
    public final String storeName;
    public final String firstName;
    public final String lastName;
    public final UserAccount applyingUser;
    
    public VendorApplicationEvent(String billingAddress, String billingCard, String storeName, String firstName, String lastName, UserAccount applyingUser) {
        this.billingAddress = billingAddress;
        this.billingCard = billingCard;
        this.storeName = storeName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.applyingUser = applyingUser;
    }
    
}
