package ie.ul.estore.user;

import ie.ul.estore.user.membership.MembershipPlan;
import ie.ul.estore.data.Model;
import ie.ul.estore.user.membership.DefaultMembership;
import ie.ul.estore.user.membership.MembershipFactory;
import java.util.ArrayList;

public class UserAccount extends Model {

    private String username;
    private String password;
    private double balance;

    private Cart cart;
    private boolean isVendor = false;
    private transient MembershipPlan membership;
    private ArrayList<Transaction> transactions;
    private String membershipPlanName;

    public UserAccount(String username, String password, double balance, String planName) {
        this.username = username;
        this.password = password;
        this.balance = balance;
        this.membershipPlanName = planName;
        this.cart = new Cart();
        this.transactions = new ArrayList<>();
        
        final MembershipFactory factory = new MembershipFactory();
        try {
            this.membership = factory.createPlanFromName(planName);
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println("Error");
        }
    }

    public UserAccount(String username, String password, double balance, String planName, Cart cart) {
        this(username, password, balance, planName);
        this.cart = cart;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addToBalance(double addBalance) {
        if (addBalance > 0.0) {
            this.balance += addBalance;
        }
    }

    public void subtractFunds(double subBalance) {
        if (subBalance > 0.0) {
            this.balance -= subBalance;
        }
    }

    public String getUsername() {
        return username;
    }

    public double getBalance() {
        return balance;
    }

    public Cart getCart() {
        return this.cart;
    }

    public Transaction[] getTransactions() {
        Transaction[] temp = transactions.toArray(new Transaction[transactions.size()]);
        return temp;
    }

    public boolean validateCredentials(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }

    public boolean isVendor() {
        return this.isVendor;
    }

    public void setVendorStatus(boolean isVendor) {
        this.isVendor = isVendor;
    }

    public void addTransaction(Transaction t) {
        this.transactions.add(t);
    }

    public MembershipPlan getMembershipPlan() {
        final MembershipFactory factory = new MembershipFactory();
        try {
            return factory.createPlanFromName(this.membershipPlanName);
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println("Error");
        }
        
        return new DefaultMembership();
    }

    public void setMembershipPlan(final MembershipPlan membership) {
        this.membershipPlanName = membership.getName();
        this.membership = membership;
    }
}
