package ie.ul.estore.user;

import ie.ul.estore.swingui.JPanelFormBuilder;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.event.Dispatcher;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class VendorApplicationView extends SwingView {

    private final JTextField billingAddress;
    private final JTextField billingCard;
    private final JTextField storeName;
    private final JTextField firstName;
    private final JTextField lastName;
    private final JButton applyButton;
    private final JPanelFormBuilder formBuilder;
    
    public VendorApplicationView(Dispatcher d, UserAccount applyingUser) {
        super(d, "Apply for Vendor Membership");
        
        formBuilder = new JPanelFormBuilder();
        
        this.billingAddress = new JTextField();
        this.billingCard = new JTextField();
        this.storeName = new JTextField();
        this.firstName = new JTextField();
        this.lastName = new JTextField();
        this.applyButton = new JButton("Apply");
        this.applyButton.addActionListener(e -> d.dispatch(new VendorApplicationEvent(
                this.billingAddress.getText(),
                this.billingCard.getText(),
                this.storeName.getText(),
                this.firstName.getText(),
                this.lastName.getText(),
                applyingUser
        )));
        
        formBuilder.add("Billing Address", billingAddress);
        formBuilder.add("Billing Card Number", billingCard);
        formBuilder.add("Shop name", storeName);
        formBuilder.add("First Name", firstName);
        formBuilder.add("Last Name", lastName);
        formBuilder.add(applyButton);
    }
    
    @Override
    public JPanel getContent() {
        return this.formBuilder.getForm();
    }
    
}
