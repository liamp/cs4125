package ie.ul.estore.user;

public class AddFundsEvent {

    public final String cardNumber;
    public final String billingAddress;
    public final String billingName;
    public final double amount;
    public final UserAccount user;

    public AddFundsEvent(String cardNumber, String billingAddress, String billingName, double amount, UserAccount user) {
        this.cardNumber = cardNumber;
        this.billingAddress = billingAddress;
        this.billingName = billingName;
        this.amount = amount;
        this.user = user;
    }

}
