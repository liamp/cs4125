package ie.ul.estore.user;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.checkout.CheckoutEvent;
import ie.ul.estore.event.Dispatcher;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AddFundsView extends SwingView {

    private final UserAccount user;

    private final JTextField cardInput;
    private final JLabel cardLabel;

    private final JTextField nameInput;
    private final JLabel nameLabel;

    private final JTextField addressInput;
    private final JLabel addressLabel;
    
    private final JTextField amountInput;
    private final JLabel amountLabel;

    private final JButton payNowButton;

    public AddFundsView(Dispatcher d, final UserAccount user) {
        super(d, "Add Funds");

        this.user = user;

        this.cardLabel = new JLabel("Card Number");
        this.addressLabel = new JLabel("Billing Address");
        this.nameLabel = new JLabel("Card Holder Name");

        super.getContent().setLayout(null);
        this.cardInput = new JTextField();
        this.nameInput = new JTextField();
        this.addressInput = new JTextField();
        this.payNowButton = new JButton("Pay now");
        
        this.amountInput = new JTextField();
        this.amountLabel = new JLabel("Amount");

        cardLabel.setBounds(80, 70, 200, 30);
        nameLabel.setBounds(80, 110, 200, 30);
        
        addressLabel.setBounds(80, 150, 200, 30);
        cardInput.setBounds(300, 70, 200, 30);
        
        nameInput.setBounds(300, 110, 200, 30);
        addressInput.setBounds(300, 150, 200, 30);
        
        amountInput.setBounds(300, 200, 200, 30);
        amountLabel.setBounds(80, 200, 200, 30);
        payNowButton.setBounds(80, 250, 100, 30);

        this.addComponent(cardLabel);
        this.addComponent(cardInput);
        this.addComponent(nameLabel);
        this.addComponent(nameInput);
        this.addComponent(addressLabel);
        this.addComponent(addressInput);
        this.addComponent(amountInput);
        this.addComponent(amountLabel);

        this.payNowButton.addActionListener(e -> {
            final String card = cardInput.getText();
            final String name = nameInput.getText();
            final String address = addressInput.getText();
            final double amount = Double.valueOf(amountInput.getText());
            final AddFundsEvent event = new AddFundsEvent(card, name, address, amount, user);
            getDispatcher().dispatch(event);
        });
        
        this.addComponent(payNowButton);
    }

}
