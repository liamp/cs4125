package ie.ul.estore.user.membership;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class MembershipFactory {
    
    private final Stream<MembershipPlan> membershipClasses;
    
    public MembershipFactory() {
        final List<MembershipPlan> classList = new ArrayList<>(3);
        classList.add(new DefaultMembership());
        classList.add(new SilverMembership());
        classList.add(new GoldMembership());

        this.membershipClasses = classList.stream();
    }
    
    public MembershipPlan createPlanFromName(final String name) throws InstantiationException, IllegalAccessException {
        final MembershipPlan plan = this.membershipClasses
                .filter(m -> m.getName().equals(name))
                .findFirst()
                .orElse(new DefaultMembership());
        return plan;
    }
}
