package ie.ul.estore.user.membership;


public class GoldMembership extends MembershipPlan {
    
    public GoldMembership() {
        super("Gold", 100.0);
    }

    @Override
    public double getModifiedPrice(double price) {
        if (price >= 300.0) return price * 0.85;
        else                return price * 0.90;
    }
    
}
