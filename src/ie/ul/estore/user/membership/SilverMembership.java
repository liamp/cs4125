package ie.ul.estore.user.membership;

public class SilverMembership extends MembershipPlan {
    
    public SilverMembership() {
        super("Silver", 70.00);
    }

    @Override
    public double getModifiedPrice(double price) {
        return price * 0.95;
    }
}
