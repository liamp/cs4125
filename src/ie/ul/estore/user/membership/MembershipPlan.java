package ie.ul.estore.user.membership;

import ie.ul.estore.data.Model;

public abstract class MembershipPlan extends Model {
    
    private final String name;
    private final double price;
    
    public MembershipPlan(String name, double price) {
        this.name = name;
        this.price = price;
    }
    
    public String getName() {
        return this.name;
    }
    
    public double getPrice() {
        return this.price;
    }
    
    public abstract double getModifiedPrice(final double price);
}
