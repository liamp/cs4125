package ie.ul.estore.user.membership;

public class DefaultMembership extends MembershipPlan{
    
    public DefaultMembership() {
        super("Default", 0.0);
    }

    @Override
    public double getModifiedPrice(double price) {
        return price;
    }
}
