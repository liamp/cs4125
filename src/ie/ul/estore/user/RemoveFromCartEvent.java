package ie.ul.estore.user;

import ie.ul.estore.product.Product;
import ie.ul.estore.user.UserAccount;


public final class RemoveFromCartEvent {
    public final UserAccount user;
    public final Product selectedProduct;
    
    public RemoveFromCartEvent(UserAccount user, Product selectedProduct) {
        this.user = user;
        this.selectedProduct = selectedProduct;
    }
    
}
