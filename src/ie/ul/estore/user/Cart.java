package ie.ul.estore.user;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

import ie.ul.estore.product.Product;

public class Cart {

    private List<Product> products;

    public Cart() {
        this.products = new ArrayList<Product>();
    }

    public void addProduct(Product p) {
        if (!this.products.contains(p)) {
            this.products.add(p);
        }
    }

    public void removeProduct(Product p) {
        if (this.products.contains(p)) {
            this.products.remove(p);
        }
    }

    public double getTotal() {
        return this.products.stream()
                .map(p -> p.getPrice())
                .reduce(0.0, (a, b) -> a + b);
    }

    public void clearCart() {
        this.products.clear();
    }

    public int getItemCount() {
        return this.products.size();
    }
    
    public Product[] getItemsInCart() {
        return this.products.toArray(new Product[0]);
    }
}
