package ie.ul.estore.user;

import ie.ul.estore.Application;
import ie.ul.estore.Controller;
import ie.ul.estore.checkout.payments.CreditCardPaymentData;
import ie.ul.estore.checkout.payments.CreditCardPaymentMethod;
import ie.ul.estore.checkout.payments.PaymentMethod;
import ie.ul.estore.checkout.payments.PaymentMethodException;
import ie.ul.estore.data.Repository;
import ie.ul.estore.event.Responds;
import ie.ul.estore.store.Store;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class UserController extends Controller {

    private final Repository<UserAccount> users;
    public UserController(Application app) {
        super(app);
        this.users = app.getUsers();
    }

    @Responds(to = AddToCartEvent.class)
    public void addProductToCart(AddToCartEvent event) {
        this.app.getLoggedInUser().getCart().addProduct(event.selectedProduct);
        try {
            users.save();
        } catch (IOException ex) {
            this.dispatchErrorMessage("Error occurred while adding product to cart");
        }
        this.dispatchInfoMessage("Added product " + event.selectedProduct.getName() + " to cart.");
    }

    @Responds(to = RemoveFromCartEvent.class)
    public void removeFromCart(RemoveFromCartEvent event) {
        this.app.getLoggedInUser().getCart().removeProduct(event.selectedProduct);
        try {
            users.save();
        } catch (IOException ex) {
            this.dispatchErrorMessage("Error occurred while removing product from cart");
        }

        this.dispatchInfoMessage("Removed product " + event.selectedProduct.getName() + " from cart.");
    }

    @Responds(to = AddFundsEvent.class)
    public void addFunds(AddFundsEvent event) {
        try {
            final PaymentMethod paymentMethod = new CreditCardPaymentMethod();
            final CreditCardPaymentData paymentData = new CreditCardPaymentData(event.user, event.amount, event.cardNumber, event.billingAddress, event.billingName);
            paymentMethod.process(paymentData);
            event.user.addToBalance(event.amount);
            users.save();
            dispatchInfoMessage("Transaction complete");
            final List<Store> storesList = app.getStores().all();
            final Store stores[] = storesList.toArray(new Store[storesList.size()]);
            dispatchRedirect(new DashboardView(app.getDispatcher(), event.user, stores));
        } catch (IllegalArgumentException | IOException e) {
            dispatchErrorMessage("Failed to add funds due to incorrect input");
        } catch (PaymentMethodException ex) {
            dispatchErrorMessage("Error ocurred: " + ex.getMessage());
        }
    }

    @Responds(to = VendorApplicationEvent.class)
    public void handleVendorApplication(VendorApplicationEvent vendorApplication) {
        Optional<UserAccount> maybeUser = this.app.getUsers().findFirst(u -> u.getId() == vendorApplication.applyingUser.getId());
        if (maybeUser.isPresent()) {
            maybeUser.get().setVendorStatus(true);
            try {
                users.save();
                dispatchRedirect(new AccountView(app.getDispatcher(), maybeUser.get()));
            } catch (IOException ex) {
                dispatchErrorMessage("We're sorry, there was a problem upgrading your account, please try again later");
            }
        }
    }

}

//repository = "users"
