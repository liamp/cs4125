package ie.ul.estore.user;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.product.CreateProductView;
import ie.ul.estore.store.Store;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.EmptyBorder;

public final class AccountView extends SwingView {

    private final JLabel accountLabel;
    private final JLabel balanceLabel;
    private final JButton addFundsButton = new JButton("Add Funds");
    private final JButton transactionHistoryButton = new JButton("Transaction History");
    private final UserAccount user;
    private final JButton createProductButton;
    private final JButton becomeVendorButton;

    public AccountView(Dispatcher d, UserAccount user) {
        super(d, "My Account");
        this.user = user;
        this.getContent().setLayout(new BorderLayout());
        final JPanel textContainer = new JPanel();
        textContainer.setLayout(new BoxLayout(textContainer, BoxLayout.Y_AXIS));

        this.accountLabel = new JLabel(user.getUsername() + "'s Account");
        this.balanceLabel = new JLabel("Balance: " + user.getBalance());
        this.accountLabel.setHorizontalAlignment(JLabel.CENTER);
        this.addFundsButton.addActionListener(e -> d.dispatch(new Redirect(new AddFundsView(d, user))));
        this.addFundsButton.setBounds(0, 0, 20, 10);
        this.becomeVendorButton = new JButton("Become a Vendor");
        this.becomeVendorButton.addActionListener(e -> d.dispatch(new Redirect(new VendorApplicationView(d, user))));
        this.createProductButton = new JButton("Create a Product");
        this.createProductButton.addActionListener(e -> d.dispatch(new Redirect(new CreateProductView(d, user))));
        
        textContainer.setBorder(new EmptyBorder(50, 10, 10, 50));
        accountLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        balanceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        transactionHistoryButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addFundsButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        textContainer.add(accountLabel);
        textContainer.add(balanceLabel);
        textContainer.add(transactionHistoryButton);
        textContainer.add(addFundsButton);
        this.addComponent(textContainer, BorderLayout.CENTER);

        if (user.isVendor()) {
            this.addComponent(createProductButton, BorderLayout.PAGE_END);
        } else {
            this.addComponent(becomeVendorButton, BorderLayout.PAGE_END);
        }
    }
}

//button addfunds;
//new addfundsevent;
//this.getdisp.disp(^^);
//put @respond to ^^.class for each event
