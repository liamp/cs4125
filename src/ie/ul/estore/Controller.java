package ie.ul.estore;

import ie.ul.estore.swingui.SwingView;


public abstract class Controller {

    protected final Application app;

    public Controller(Application app) {
        this.app = app;
    }
    
    protected void dispatchErrorMessage(String errorMessage) {
        this.app.getDispatcher().dispatch(new MessageEvent(MessageEvent.ERROR, errorMessage));
    }
    
    protected void dispatchInfoMessage(String infoMessage) {
        this.app.getDispatcher().dispatch(new MessageEvent(MessageEvent.INFORMATION, infoMessage));        
    }
    
    protected void dispatchRedirect(final SwingView v) {
        this.app.getDispatcher().dispatch(new Redirect(v));
    }
}
