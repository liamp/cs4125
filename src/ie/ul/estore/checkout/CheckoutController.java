package ie.ul.estore.checkout;

import ie.ul.estore.Application;
import ie.ul.estore.Controller;
import ie.ul.estore.user.membership.MembershipPlan;
import ie.ul.estore.user.Transaction;
import ie.ul.estore.event.Responds;
import java.io.IOException;
import ie.ul.estore.user.UserAccount;
import java.util.Date;

public class CheckoutController extends Controller {

    public CheckoutController(Application app) {
        super(app);
    }
    
    @Responds(to = CheckoutEvent.class)
    public void checkout(CheckoutEvent checkout) {
        final UserAccount user = checkout.user;
        final MembershipPlan userPlan = user.getMembershipPlan();
        final double purchaseTotal = userPlan.getModifiedPrice(user.getCart().getTotal());

        if (purchaseTotal < user.getBalance()) {
            try {
                final Date currentDate = new Date();
                final Transaction purchaseTransaction = new Transaction(currentDate, purchaseTotal);
                app.getTransactions().insert(purchaseTransaction);
                user.addTransaction(purchaseTransaction);
                user.getCart().clearCart();
                user.subtractFunds(purchaseTotal);
                app.getUsers().save();
            } catch (IOException ex) {
                dispatchErrorMessage("An application error ocurred while performning your transaction. Please try again later.");
            }
        } else {
            dispatchErrorMessage("Insufficient Funds" + purchaseTotal + " vs " + user.getBalance());
        }
    }
    
}
