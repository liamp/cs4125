package ie.ul.estore.checkout;

import ie.ul.estore.user.UserAccount;

public final class CheckoutEvent {
    public final UserAccount user;
    
    public CheckoutEvent(final UserAccount user) {
        this.user = user;
    }
}
