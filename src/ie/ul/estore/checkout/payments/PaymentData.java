package ie.ul.estore.checkout.payments;

import ie.ul.estore.user.UserAccount;

public abstract class PaymentData {
    private final UserAccount user;
    private final double amount;
    
    protected PaymentData(UserAccount user, double amount) {
        this.user = user;
        this.amount = amount;
    }
    
    protected UserAccount getUser() {
        return this.user;
    }
    
    protected double getAmount() {
        return this.amount;
    }
}
