package ie.ul.estore.checkout.payments;

import ie.ul.estore.checkout.payments.PaymentData;
import ie.ul.estore.user.UserAccount;

public class CreditCardPaymentData extends PaymentData {

    private final String cardNumber;
    private final String billingAddress;
    private final String billingName;

    public CreditCardPaymentData(UserAccount user, double amount, String cardNumber, String billingAddress, String billingName) {
        super(user, amount);

        this.cardNumber = cardNumber;
        this.billingAddress = billingAddress;
        this.billingName = billingName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public String getBillingName() {
        return billingName;
    }

}
