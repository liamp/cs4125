package ie.ul.estore.checkout.payments;

import ie.ul.estore.checkout.payments.CreditCardPaymentData;
import ie.ul.estore.checkout.payments.PaymentMethod;
import ie.ul.estore.checkout.payments.PaymentMethodException;

public class CreditCardPaymentMethod implements PaymentMethod<CreditCardPaymentData> {

    @Override
    public void process(CreditCardPaymentData data) throws PaymentMethodException {
        if (CreditCardPaymentMethod.luhnAlgorthim(data.getCardNumber())) {
            // Dummy logic here for sending the data to external credit card
            // providers (e.g. Visa,Mastercard etc...)
        } else {
            throw new PaymentMethodException("Invalid card number");
        }
    }

    private static boolean luhnAlgorthim(String cardNo) {
        int nDigits = cardNo.length();

        int nSum = 0;
        boolean isSecond = false;
        for (int i = nDigits - 1; i >= 0; i--) {

            int d = cardNo.charAt(i) - '0';

            if (isSecond == true) {
                d *= 2;
            }
            nSum += d / 10;
            nSum += d % 10;

            isSecond = !isSecond;
        }
        return (nSum % 10 == 0);

    }

}
