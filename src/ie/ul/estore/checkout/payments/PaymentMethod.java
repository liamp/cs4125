package ie.ul.estore.checkout.payments;

public interface PaymentMethod<T extends PaymentData> {
    void process(T paymentData) throws PaymentMethodException;
}
