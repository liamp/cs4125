package ie.ul.estore.checkout.payments;


public class PaymentMethodException extends Exception {
    public PaymentMethodException(String errorMessage) {
        super(errorMessage);
    }
}
