package ie.ul.estore.checkout;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import ie.ul.estore.user.membership.MembershipPlan;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CheckoutView extends SwingView {
    
    private final UserAccount user;
    
    private final JTextField prevBalance;
    private final JLabel prevBalanceLabel;
    
    private final JTextField purchase;
    private final JLabel purchaseAmountLabel;
    
    private final JTextField newBalance;
    private final JLabel newBalanceLabel;
    
    private final JButton payNowButton;
    
    public CheckoutView(Dispatcher d, UserAccount user) {
        super(d, "Checkout");
        
        this.user = user;
        
        this.prevBalanceLabel = new JLabel("Current Balance");
        this.newBalanceLabel = new JLabel("New Balance");
        this.purchaseAmountLabel = new JLabel("Purchase Amount");
        
        super.getContent().setLayout(null);
        this.prevBalance = new JTextField("€" + user.getBalance());
        this.purchase = new JTextField("€" + user.getCart().getTotal());        
        this.newBalance = new JTextField("€x" + (user.getBalance() - user.getCart().getTotal()));
        this.payNowButton = new JButton("Pay now");

        prevBalanceLabel.setBounds(80, 70, 200, 30);
        purchaseAmountLabel.setBounds(80, 110, 200, 30);
        newBalanceLabel.setBounds(80, 150, 200, 30);
        prevBalance.setBounds(300, 70, 200, 30);
        purchase.setBounds(300, 110, 200, 30);
        newBalance.setBounds(300, 150, 200, 30);
        payNowButton.setBounds(300, 200, 100, 30);
        
        this.addComponent(prevBalanceLabel);
        this.addComponent(prevBalance);
        this.addComponent(purchaseAmountLabel);
        this.addComponent(purchase);
        this.addComponent(newBalanceLabel);
        this.addComponent(newBalance);
        
        this.payNowButton.addActionListener(e -> {
            final String card = prevBalance.getText();
            final String name = purchase.getText();
            final String address = newBalance.getText();
            final CheckoutEvent checkout = new CheckoutEvent(user);
            getDispatcher().dispatch(checkout);
        });
        this.addComponent(payNowButton);
    }
    
    
    
}
