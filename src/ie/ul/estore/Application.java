package ie.ul.estore;

import ie.ul.estore.user.Transaction;
import ie.ul.estore.swingui.MainWindow;
import ie.ul.estore.store.StoreController;
import ie.ul.estore.store.Store;
import ie.ul.estore.data.JsonRepository;
import ie.ul.estore.data.Repository;
import ie.ul.estore.event.Dispatcher;

import ie.ul.estore.auth.LoginView;
import ie.ul.estore.auth.AuthController;
import ie.ul.estore.checkout.CheckoutController;
import ie.ul.estore.user.UserAccount;
import ie.ul.estore.user.UserController;

public final class Application {

    private final Repository<UserAccount> users;
    private final Repository<Store> stores;
    private final Repository<Transaction> transactions;
    private final MainWindow window;
    private final Dispatcher eventDispatcher;

    private final AuthController authController;
    private final StoreController storeController;
    private final CheckoutController checkoutController;
    private final UserController userController;

    private UserAccount loggedInUser;

    public UserAccount getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(final UserAccount user) {
        this.loggedInUser = user;
    }

    private Application(Dispatcher dispatcher) throws Exception {
        this.eventDispatcher = dispatcher;

        this.users = new JsonRepository<>(UserAccount.class, "users.json");
        this.stores = new JsonRepository<>(Store.class, "stores.json");
        this.transactions = new JsonRepository<>(Transaction.class, "transactions.json");

        this.authController = new AuthController(this, this.users);
        this.storeController = new StoreController(this);
        this.checkoutController = new CheckoutController(this);
        this.userController = new UserController(this);

        this.window = new MainWindow("UL E-Store", new LoginView(this.eventDispatcher), dispatcher, this);
        this.eventDispatcher.register(this.window);
        this.eventDispatcher.register(this.authController);
        this.eventDispatcher.register(this.storeController);
        this.eventDispatcher.register(this.checkoutController);
        this.eventDispatcher.register(this.userController);
    }

    public Repository<UserAccount> getUsers() {
        return this.users;
    }

    public Repository<Store> getStores() {
        return this.stores;
    }

    public Repository<Transaction> getTransactions() {
        return this.transactions;
    }

    public Dispatcher getDispatcher() {
        return this.eventDispatcher;
    }

    public static void main(String args[]) {
        try {
            final Dispatcher d = new Dispatcher();
            final Application app = new Application(d);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
