package ie.ul.estore.swingui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class JPanelFormBuilder {
    
    private final JPanel form;
    private final GridBagConstraints constraints;
    
    public JPanelFormBuilder() {
        this.form = new JPanel(new GridBagLayout());
        this.constraints = new GridBagConstraints();
        this.constraints.fill = GridBagConstraints.HORIZONTAL;
        this.constraints.anchor = GridBagConstraints.CENTER;
        this.constraints.insets = new Insets(5, 5, 5, 5);
        this.constraints.weighty = 0.4;
        this.form.setBorder(new EmptyBorder(200, 200, 200, 200));
    }
    
    public void add(String label, final Component component) {
        ++this.constraints.gridy;
        this.constraints.gridx = 0;
        this.constraints.weightx = 0.5;  
        this.form.add(new JLabel(label), this.constraints);
        this.constraints.gridx = 1;
        this.constraints.weightx = 1;
        this.form.add(component, this.constraints);        
    }
    
    public void add(final Component component) {
        ++this.constraints.gridy;
        this.constraints.gridx = 0;
        this.form.add(component, this.constraints);         
    }
    
    public JPanel getForm() {
        return this.form;
    }
}
