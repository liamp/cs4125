package ie.ul.estore.swingui;

import ie.ul.estore.event.Dispatcher;
import java.awt.Component;
import javax.swing.JPanel;

public abstract class SwingView {

    private final JPanel content;
    private final Dispatcher dispatcher;
    private final String title;
    
    public SwingView(Dispatcher d, String title) {

        this.dispatcher = d;
        this.title = title;
        this.content = new JPanel();
        this.content.setLayout(null);
    }

    protected final void addComponent(Component comp) {
        this.content.add(comp);
    }

    protected final void addComponent(Component comp, Object constraints) {
        this.content.add(comp, constraints);
    }
    
    protected Dispatcher getDispatcher() {
        return this.dispatcher;
    }
    
    public JPanel getContent() {
        return this.content;
    }
    
    public String getTitle() {
        return this.title;
    }
}
