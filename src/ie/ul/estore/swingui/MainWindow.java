package ie.ul.estore.swingui;

import ie.ul.estore.Application;
import ie.ul.estore.MessageEvent;
import ie.ul.estore.Redirect;
import ie.ul.estore.user.CartView;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.event.Responds;
import java.awt.BorderLayout;
import java.awt.Container;
import java.util.Stack;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

public final class MainWindow {
    private final JFrame window;
    private final Dispatcher dispatcher;
    
    private SwingView currentView;
    private final JToolBar headerBar;
    private final Container mainContent;
    
    private final Stack<SwingView> viewHistory;
    private final JButton backButton;
    private final JLabel title;
    private final JButton cartButton;
    
    private JToolBar createToolBar(JButton back, JLabel title, JButton cart) {
        final JToolBar toolbar = new JToolBar();
        toolbar.setLayout(new BorderLayout());
        toolbar.add(back, BorderLayout.WEST);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        toolbar.add(title, BorderLayout.CENTER);
        toolbar.add(cart, BorderLayout.EAST);
        toolbar.setFloatable(false);
        
        return toolbar;
    }
    
    public MainWindow(final String title, final SwingView initialView, final Dispatcher dispatcher, final Application app) {
        this.window = new JFrame(title);
        this.window.setSize(800, 600);
        this.window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        this.window.getContentPane().setLayout(new BorderLayout());
        
        this.backButton = new JButton("Go Back");
        this.backButton.addActionListener(e -> this.goBack());
        
        this.title = new JLabel(initialView.getTitle());
        this.cartButton = new JButton("Cart");
        this.cartButton.addActionListener(e -> dispatcher.dispatch(new Redirect(new CartView(dispatcher, app.getLoggedInUser()))));
        
        this.headerBar = this.createToolBar(this.backButton, this.title, this.cartButton);
        this.window.getContentPane().add(this.headerBar, BorderLayout.PAGE_START);
        
        this.mainContent = new JPanel();
        this.mainContent.setLayout(new BorderLayout());
        this.mainContent.add(initialView.getContent(), BorderLayout.CENTER);
        
        this.window.getContentPane().add(this.mainContent, BorderLayout.CENTER);
        this.window.setVisible(true);
        
        this.dispatcher = dispatcher;
        this.currentView = initialView;
        
        this.viewHistory = new Stack<>();
    }
    
    @Responds(to = Redirect.class)
    public void setContent(Redirect next) {
        this.viewHistory.push(this.currentView);
        this.setView(next.view);
    }
    
    @Responds(to = MessageEvent.class)
    public final void showMessageDialog(MessageEvent msg) {   
        int icon = JOptionPane.INFORMATION_MESSAGE;
        switch (msg.type) {
            case MessageEvent.ERROR:
                icon = JOptionPane.ERROR_MESSAGE;
                break;
            case MessageEvent.WARNING:
                icon = JOptionPane.WARNING_MESSAGE;
                break;
        }
        
        JOptionPane.showMessageDialog(null, msg.message, "", icon);
    }
    
    @Responds(to = Redirect.Back.class)
    public void goBack() {
        if (!this.viewHistory.isEmpty()) {
            final SwingView lastView = this.viewHistory.pop();
            this.setView(lastView);
        }
    }

    private void setView(final SwingView view) {
        this.dispatcher.unregister(this.currentView);
        this.mainContent.removeAll();
        this.mainContent.add(view.getContent(), BorderLayout.CENTER);
        this.currentView = view;
        this.dispatcher.register(this.currentView);
        this.window.revalidate();
        this.title.setText(view.getTitle());
        this.window.repaint();        
    }
}
