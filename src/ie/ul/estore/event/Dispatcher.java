package ie.ul.estore.event;

import java.util.Map;
import java.util.HashMap;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Handles dispatching events to a list of classes that promise they can
 * handle them.
 * 
 * Internally, the list of responders is maintained as a map from the event type
 * to an (Object, Method) tuple. This layout makes insertions more expensive but
 * iterations faster, as we can do a single lookup for the type of event we're
 * currently handling and just iterate through the responders.
 * 
 * @author Liam
 */
public class Dispatcher {
    private final ConcurrentMap<Class<?>, Map<Object, Method>> responders;

    public Dispatcher() {
        this.responders = new ConcurrentHashMap<>();
    }

    public void register(Object responder) {
        Class<?> c = responder.getClass();
        while (c != Object.class) {
            final Map<Object, Method> responderMethods = new HashMap<>();
            for (Method m : c.getDeclaredMethods()) {
                if (m.isAnnotationPresent(Responds.class)) {
                    
                    final Class<?> eventType = m.getAnnotation(Responds.class).to();
                    // If we don't have an entry for this event type already, 
                    // insert a new one. Otherwise, inser this responder into the
                    // corresponding event key.
                    if (!this.responders.containsKey(eventType)) {
                        this.responders.put(eventType, new HashMap<>());
                    }
                    this.responders.get(eventType).put(responder, m);
                }
            }

            c = c.getSuperclass();
        }
    }

    public void unregister(final Object responder) {
        for (Map<Object, Method> eventResponders : this.responders.values()) {
            if (eventResponders.containsKey(responder)) {
                eventResponders.remove(responder);
            }
        }
    }
    
    public void dispatch(final Object event) {
        if (this.responders.containsKey(event.getClass())) {
          try {
                final Map<Object, Method> eventResponders = this.responders.get(event.getClass());
                for (Map.Entry<Object, Method> responder : eventResponders.entrySet()) {
                    responder.getValue().invoke(responder.getKey(), event);
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                ex.printStackTrace();
            }          
        }
    }
}
