package ie.ul.estore.data;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class Model
{
    private static final AtomicInteger AUTO_INCREMENTER = new AtomicInteger();
    private final int id;
    
    public Model() {
        this.id = AUTO_INCREMENTER.getAndIncrement();
    }
    
    public int getId() {
        return this.id;
    }
}