package ie.ul.estore.data;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Represents an abstraction over the storage of a collection of Models.
 * 
 * A repository allows us to abstract away the differences in implementations of
 * model storage. Client code need not concern itself with whether the application
 * uses JSON, flat files, a relational database or any other scheme for data
 * storage.
 * 
 * @author Liam
 * @param <M> The model this repository is storing
 */
public interface Repository<M extends Model> {

    Optional<M> findFirst(Predicate<M> query);

    Stream<M> findAll(Predicate<M> query);

    void insert(M m) throws IOException;

    void delete(M m) throws IOException;

    void update(Consumer<M> transformer) throws IOException;

    void save() throws IOException;

    List<M> all();
}
