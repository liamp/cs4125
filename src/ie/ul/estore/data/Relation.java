package ie.ul.estore.data;

import java.io.Serializable;
import java.util.function.Predicate;

public final class Relation<M extends Model> implements Serializable, Predicate<M> {

    private final int destID;

    public Relation(Model dest) {
        this.destID = dest.getId();
    }

    @Override
    public boolean test(M t) {
        return t.getId() == this.destID;
    }

    public static <T extends Model> Relation<T> hasOne(final T m) {
        return new Relation<>(m);
    }
}
