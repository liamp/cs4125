package ie.ul.estore;

public final class MessageEvent {
    public static final byte ERROR = 0x1;
    public static final byte WARNING = 0x2;
    public static final byte INFORMATION = 0x4;
    
    public final byte type;
    public final String message;
    
    public MessageEvent(final byte type, final String message) {
        this.type = type;
        this.message = message;
    }
}
