package ie.ul.estore.product;

import ie.ul.estore.data.Model;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Product extends Model {

    private String name;
    private final String description;
    private double price;
    private final List<Review> reviews;
    private final List<String> categoryNames;

    public Product(String name, double price, String description, List<String> categoryNames) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.categoryNames = categoryNames;
        this.reviews = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        String temp = name + "," + price;
        return temp;
    }

    public String getDescription() {
        return this.description;
    }

    public void addReview(final Review r) {
        this.reviews.add(r);
    }

    public Review getReview(int i) {
        return this.reviews.get(i);
    }

    public List<Review> getReviews() {
        return Collections.unmodifiableList(this.reviews);
    }

    public List<Review> getHighestReviews(int maxHighest) {
        final List<Review> topReviews = this.reviews.stream()
                .sorted()
                .limit(maxHighest)
                .collect(Collectors.toList());
        return Collections.unmodifiableList(topReviews);
    }

    public double getRating() {
        if (this.reviews.isEmpty()) {
            return 0.0;
        } else {
            double temp = reviews.get(0).getRating();
            for (int i = 0; i < this.reviews.size(); i++) {
                temp = (temp + this.reviews.get(i).getRating()) / 2;
            }

            return temp;
        }
    }

    public List<String> getCategories() {
        return Collections.unmodifiableList(this.categoryNames);
    }
}
