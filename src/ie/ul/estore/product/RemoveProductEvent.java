package ie.ul.estore.product;

import ie.ul.estore.user.UserAccount;

/**
 *
 * @author Eric Nolan
 */
public class RemoveProductEvent {
    public final UserAccount removingUser;
    public final Product product;
    
    public RemoveProductEvent(final UserAccount removingUser, final Product p) {
        this.removingUser = removingUser;
        this.product = p;
    }
}
