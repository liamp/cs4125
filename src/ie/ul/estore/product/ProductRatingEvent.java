package ie.ul.estore.product;

import ie.ul.estore.user.UserAccount;

public final class ProductRatingEvent {
    public final String reviewText;
    public final UserAccount author;
    public final Product product;
    public final double rating;    

    public ProductRatingEvent(String reviewText, UserAccount author, double rating, Product product) {
        this.reviewText = reviewText;
        this.author = author;
        this.rating = rating;
        this.product = product;
    }
}
