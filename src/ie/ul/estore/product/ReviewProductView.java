package ie.ul.estore.product;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;

public class ReviewProductView extends SwingView {

    private final Product product;

    private final JComboBox rating;
    private final JTextArea review;
    private final JButton confirmReview;

    public ReviewProductView(Dispatcher d, UserAccount user, Product product) {
        super(d, "Add a review");
        this.product = product;

        this.getContent().setLayout(new BoxLayout(this.getContent(), BoxLayout.Y_AXIS));

        this.review = new JTextArea();
        this.rating = new JComboBox();
        this.confirmReview = new JButton("Confirm");
        this.rating.addItem("1.Bad");
        this.rating.addItem("2.Okay");
        this.rating.addItem("3.Good");
        this.rating.addItem("4.Very Good");
        this.rating.addItem("5.Excellent");

        confirmReview.addActionListener(e -> getDispatcher().dispatch(new ProductRatingEvent(review.getText(), user, rating.getSelectedIndex() + 1, product)));

        this.addComponent(this.rating);
        this.addComponent(this.review);
        this.addComponent(this.confirmReview);
    }
}
