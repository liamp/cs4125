package ie.ul.estore.product;

import ie.ul.estore.data.Relation;
import ie.ul.estore.user.UserAccount;

public final class Review {
    private final String reviewText;
    private final Relation<UserAccount> author;
    private final double rating;
    
    public Review(String text, UserAccount author, double rating) {
        this.reviewText = text;
        this.author = Relation.hasOne(author);
        this.rating = rating;
    }
    
    public String getReviewText() {
        return this.reviewText;
    }
    
    public double getRating() {
        return this.rating;
    }
    
    public Relation<UserAccount> getAuthor() {
        return this.author;
    }
    
}
