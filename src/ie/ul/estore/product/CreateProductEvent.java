package ie.ul.estore.product;

import ie.ul.estore.user.UserAccount;
import java.util.Arrays;
import java.util.List;

public class CreateProductEvent {
    public final String name;
    public final String description;
    public final double price;
    public final UserAccount addingUser;
    public final List<String> categoryNames;
    
    public CreateProductEvent(final String name, final String description, final double price, final UserAccount addingUser, final String categoryNames[]) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.addingUser = addingUser;
        this.categoryNames = Arrays.asList(categoryNames);
    }
}
