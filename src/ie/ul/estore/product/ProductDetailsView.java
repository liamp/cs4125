package ie.ul.estore.product;

import ie.ul.estore.user.AddToCartEvent;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;

public class ProductDetailsView extends SwingView {

    private final Product product;
    
    private final JLabel productName;
    private final JLabel productDesc;
    private final JLabel productPrice;
    private final JButton addToCartButton;
    private final JButton reviewProductButton;
    private final JLabel rating;
    private final JLabel categories;
    private final JList topReviews;
    private final List<Review> reviewList;
    private final JButton allReviewsButton;
    private final JLabel reviewTitle;
    private final JButton removeProductButton;
    
    public ProductDetailsView(Dispatcher d, UserAccount user, Product p) {
        super(d, p.getName());
        
        this.product = p;
        this.reviewList = p.getReviews();
        super.getContent().setLayout(new BoxLayout(super.getContent(), BoxLayout.Y_AXIS));
        
        this.productName = new JLabel(p.getName());
        this.productDesc = new JLabel(p.getDescription());
        this.productPrice = new JLabel("€" + p.getPrice());
        this.addToCartButton = new JButton("Add to Cart");
        this.reviewProductButton = new JButton("Review Product");
        this.rating = new JLabel("Product Rating: " + p.getRating());
        this.categories = new JLabel("Categories:" + p.getCategories());

        this.reviewTitle = new JLabel("\n \n \n REVIEWS");
        this.topReviews = new JList(p.getHighestReviews(3).toArray());
        this.allReviewsButton = new JButton("All Reviews");
        
        this.addToCartButton.addActionListener(e -> d.dispatch(new AddToCartEvent(user, p)));
        this.reviewProductButton.addActionListener(e -> d.dispatch(new Redirect(new ReviewProductView(d,user, p))));
        this.allReviewsButton.addActionListener(e -> d.dispatch(new Redirect(new AllReviewsView(d,user, p))));
        
        this.removeProductButton = new JButton("Remove Product");
        this.removeProductButton.addActionListener(e -> d.dispatch(new RemoveProductEvent(user, p)));
        
        this.addComponent(productName);
        this.addComponent(addToCartButton);
        this.addComponent(reviewProductButton);
        this.addComponent(rating);
        this.addComponent(productPrice);
        this.addComponent(productDesc);
        this.addComponent(reviewTitle);
        this.addComponent(topReviews);
        this.addComponent(allReviewsButton);
        this.addComponent(removeProductButton);
    }
    
}
