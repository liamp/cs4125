package ie.ul.estore.product;

import ie.ul.estore.user.AddToCartEvent;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.Redirect;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

public class AllReviewsView extends SwingView {

    private final Product product;
    private final List<Review> reviewList;
    
    public AllReviewsView(Dispatcher d, UserAccount user, Product p) {
        super(d, "All Reviews");
        
        this.product = p;
        this.reviewList = p.getReviews();
        this.getContent().setLayout(new BoxLayout(this.getContent(), BoxLayout.Y_AXIS));
        
        for(int i=0;i<reviewList.size();i++){
        JLabel temp = new JLabel(reviewList.get(i).getReviewText());
        this.getContent().add(temp);
        }
    }
}
