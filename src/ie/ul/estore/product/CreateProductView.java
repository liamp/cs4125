package ie.ul.estore.product;

import ie.ul.estore.swingui.JPanelFormBuilder;
import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public final class CreateProductView extends SwingView {
    
    private final JPanelFormBuilder formBuilder;
    
    private final JTextField nameInput;
    private final JTextArea descInput;
    private final JTextField priceInput;
    private final JTextField categoriesInput;
    private final JButton addProductButton;
    
    public CreateProductView(Dispatcher d, UserAccount owner) {
        super(d, "Create a product");
        this.nameInput = new JTextField();
        this.descInput = new JTextArea();
        this.priceInput = new JTextField();
        this.categoriesInput = new JTextField();
        this.addProductButton = new JButton("Add Product");
        
        this.addProductButton.addActionListener(e -> d.dispatch(new CreateProductEvent(
                nameInput.getText(), 
                descInput.getText(), 
                Double.valueOf(priceInput.getText()),
                owner,
                categoriesInput.getText().split(",")
        )));
        
        this.formBuilder = new JPanelFormBuilder();
        formBuilder.add("Product Name", nameInput);
        formBuilder.add("Product Description", descInput);
        formBuilder.add("Price (€)", priceInput);
        formBuilder.add("Categories (comma separated)", categoriesInput);
        formBuilder.add(addProductButton);
    }
    
    @Override
    public JPanel getContent() {
        return this.formBuilder.getForm();
    }
}

