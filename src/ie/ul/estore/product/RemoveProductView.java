package ie.ul.estore.product;

import ie.ul.estore.swingui.SwingView;
import ie.ul.estore.user.RemoveFromCartEvent;
import ie.ul.estore.event.Dispatcher;
import ie.ul.estore.user.UserAccount;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

public class RemoveProductView extends SwingView {

    private final Product product;
    
    private final JLabel productName;
    private final JButton removeFromCartButton;
    
    public RemoveProductView(Dispatcher d, UserAccount user, Product p) {
        super(d, p.getName());
        
        this.product = p;
        this.getContent().setLayout(new BoxLayout(this.getContent(), BoxLayout.Y_AXIS));
        
        this.productName = new JLabel(p.getName());
        this.removeFromCartButton = new JButton("Remove From Cart");
        
        this.removeFromCartButton.addActionListener(e -> d.dispatch(new RemoveFromCartEvent(user, p)));
        
        this.getContent().add(this.productName);
        this.getContent().add(this.removeFromCartButton);
    }
    
}
